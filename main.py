import csv
import os
from prettytable import PrettyTable
from operator import itemgetter

# %%
base_file = os.path.join(os.getcwd(), 'base_file.csv')


# Функция записи результата работы в CSV
def csv_writer(data, path):
    """
    Write data to a CSV file path
    """
    with open(path, "w", newline='', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
        for line in data:
            writer.writerow(line)


# %%
# Функция вывода таблицы со студентами
def print_pretty_table(list_of_data):
    x = PrettyTable(list_of_data[0])
    for line in list_of_data[1:]:
        x.add_row(line)
    print(x)


# %%
# Функция ввода данных студентов
def insert_data(fio, group_name, gender, bithdate, city, data_list):
    while True:
        save_choice = input(
            """\nВы ввели следующую информацию:\n
            ФИО: {fio}
            Группа: {group_name}
            Пол: {gender}
            ДР: {bithdate}
            Город: {city}\n
            Сохранить? (yes / no) \n""".format(fio=fio,
                                               group_name=group_name,
                                               gender=gender,
                                               bithdate=bithdate,
                                               city=city))
        if not (save_choice.lower() == "yes" or save_choice.lower() == "no"):
            print('Внимание! Необходимо ввести "Yes" или "No"!!! \n')
            continue
        elif save_choice.lower() == "yes":
            print('Запись добавлена в базу!!! \n')
            max_id = max([int(l[0]) for l in data_list[1:]])
            line_data = [max_id + 1, fio, group_name, gender, bithdate, city]
            data_list.append(line_data)
            print_pretty_table(data_list)
            break
        else:
            break


def get_data(data_list):
    fio = input("Введите ФИО студента ")
    group_name = input("Введите группу студента ")
    gender = input("Введите пол студента ")
    bithdate = input("Введите день рождения студента ")
    city = input("Введите город студента ")
    insert_data(fio, group_name, gender, bithdate, city, data_list)


def find_by_id(data_list):
    stud_id = int(input("Введите ID студента"))
    stud_line = [data_list[0]] + [line for line in data_list[1:] if int(line[0]) == stud_id]
    del_index = [index for index, line in enumerate(data_list[1:], 1) if int(line[0]) == stud_id][0]
    print_pretty_table(stud_line)
    return del_index


def find_by_fio(data_list):
    stud_fio = input("Введите ФИО студента")
    stud_line = [data_list[0]] + [line for line in data_list[1:] if stud_fio.lower() in line[1].lower()]
    stud_ids = [int(line[0]) for line in data_list[1:] if stud_fio.lower() in line[1].lower()]
    print_pretty_table(stud_line)
    return stud_ids


def find_by_group_name(data_list):
    stud_group_name = input("Введите номер группы студента")
    stud_line = [data_list[0]] + [line for line in data_list[1:] if stud_group_name.lower() in line[2].lower()]
    stud_ids = [int(line[0]) for line in data_list[1:] if stud_group_name.lower() in line[2].lower()]
    print_pretty_table(stud_line)
    return stud_ids


# %%
with open(base_file, encoding='utf-8') as file:
    temp_info = csv.reader(file, delimiter=';', quotechar='"')
    temp_info = [row for row in temp_info]
    temp_info = [temp_info[0]] + [[int(num), fio, group_name, gender, birthdate, city]
                                  for num, fio, group_name, gender, birthdate, city in temp_info[1:]]

# %%

while True:
    print_pretty_table(temp_info)
    # - Get value
    choice = input("Вы хотите отсортировать записи или добавить/удалить запись? (yes / no) \n ")
    if not (choice.lower() == "yes" or choice.lower() == "no"):
        print('Внимание! Необходимо ввести "Yes" или "No"!!! \n')
        continue
    elif choice.lower() == "yes":
        while True:
            choice2 = input('Для добавление введите "add"\n'
                            'Для удаления введите "del"\n'
                            'Для сортировки по id наберите "id sort"\n'
                            'Для сортировки по ФИО наберите "fio sort"\n'
                            'Для сортировки по Группе наберите "group sort"\n')
            if not (choice2.lower() == "add"
                    or choice2.lower() == "del"
                    or choice2.lower() == "id sort"
                    or choice2.lower() == "fio sort"
                    or choice2.lower() == "group sort"):
                print('Внимание! Необходимо ввести одно из следующих значений: "add" / "del" / "change"!!! \n')
                continue

            elif choice2.lower() == "add":  # Блок на добавление студента
                get_data(temp_info)
                while True:
                    another_one_chioce = input('Добавить еще одного студента? (yes / no) \n')
                    if not (another_one_chioce.lower() == "yes" or another_one_chioce.lower() == "no"):
                        print('Внимание! Необходимо ввести "Yes" или "No"!!! \n')
                        continue
                    elif another_one_chioce.lower() == "yes":
                        get_data(temp_info)
                    else:
                        break
                break

            elif choice2.lower() == "del":  # Блок на удаление
                while True:
                    get_key_for_del = input("\nКак будем искать студента, которого надо удалить?\n"
                                            'Для поиска по ID введите "by id"\n'
                                            'Для поиска по фамилии введите "by fio" \n'
                                            'Для поиска по группе введите "by gn" ')
                    if not (get_key_for_del.lower() == "by id"
                            or get_key_for_del.lower() == "by fio"
                            or get_key_for_del.lower() == "by gn"):
                        print('Внимание! Необходимо ввести "by id" / "by fio" / "by gn"!!! \n')
                        continue
                    elif get_key_for_del.lower() == "by id":
                        del_line = find_by_id(temp_info)
                        while True:
                            del_query = input("Удалить запись? (yes / no)\n ")
                            if not (del_query.lower() == "yes" or del_query.lower() == "no"):
                                print('Внимание! Необходимо ввести "Yes" или "No"!!! \n')
                                continue
                            elif del_query.lower() == "yes":
                                del temp_info[del_line]
                                break
                            else:
                                break
                        break
                    elif get_key_for_del.lower() == "by fio":
                        del_ids = find_by_fio(temp_info)
                        del_line = find_by_id(temp_info)
                        while True:
                            del_query = input("Удалить запись? yes / no ")
                            if not (del_query.lower() == "yes" or del_query.lower() == "no"):
                                print('Внимание! Необходимо ввести "Yes" или "No"!!! \n')
                                continue
                            elif del_query.lower() == "yes":
                                del temp_info[del_line]
                                break
                            else:
                                break
                        break
                    elif get_key_for_del.lower() == "by gn":
                        del_ids = find_by_group_name(temp_info)
                        del_line = find_by_id(temp_info)
                        while True:
                            del_query = input("Удалить запись? yes / no ")
                            if not (del_query.lower() == "yes" or del_query.lower() == "no"):
                                print('Внимание! Необходимо ввести "Yes" или "No"!!! \n')
                                continue
                            elif del_query.lower() == "yes":
                                del temp_info[del_line]
                                break
                            else:
                                break
                        break

                    else:
                        break
                break

            elif choice2.lower() == "id sort":  # Блок на удаление
                temp_info = [temp_info[0]] + sorted(temp_info[1:], key=itemgetter(0))
                break

            elif choice2.lower() == "fio sort":  # Блок на удаление
                temp_info = [temp_info[0]] + sorted(temp_info[1:], key=itemgetter(1))
                break

            elif choice2.lower() == "group sort":  # Блок на удаление
                temp_info = [temp_info[0]] + sorted(temp_info[1:], key=itemgetter(2))
                break

            else:
                break

    else:
        print('Спасибо, всего доброго!')
        csv_writer(temp_info, base_file)
        break